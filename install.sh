DIR=/usr/local/share/coverThumbnail
BIN=/usr/local/bin

mkdir -p $DIR || exit 1
cp ./vinyl.webp $DIR/ || exit 1
cp ./coverThumbnail $BIN/ || exit 1
cp ./audioCoverArt.thumbnailer /usr/share/thumbnailers/ || exit 1

